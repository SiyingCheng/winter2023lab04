public class Blender{
	//public fileds 
	private String brands;
	private int price;
	private String color;
	double discount;
	
	//constructor that take 3 parameters
	public Blender (String brands,int price,String color){
		this.brands = brands;
		this.price = price;
		this.color = color;
	}
	
	//for private fields
	public void setBrands(String newBrands){
		this.brands = newBrands ;
	}
	
	public void setPrice(int newPrice){
		this.price = newPrice ;
	}
	
	public void setColor(String newColor){
		this.color = newColor ;
	}
	
	//set method for these private fields
	public String getBrands(){
		return this.brands;
	}
	
	public int getPrice(){
		return this.price;
	}
	
	public String getColor(){
		return this.color;
	}
	
	// instance method to prind 3 brands of Blender
	public void printBrands(){
		System.out.println (brands);
	}
	
	// instance method to make different smoothie according the input of colors.
	public void makeSmoothie(){
		if (this.color.equals("pink")){
			System.out.println("You wil have a Strawberry Smoothie :)");
		}
		else if (this.color.equals("yellow")){
			System.out.println("You will have a Mango Smoothie :/");
		}
		else if (this.color.equals("blue")){
			System.out.println("You will have a Blueberry Smoothie :0");
		}
		else {
			System.out.println("Meh! Regular Smoothie");	
		}
			
	}
	//instance method take 1 paraneter to do discount of blender price
	public void getDiscount (double discount){
		double finalPrice = this.price-this.price*discount;	
		System.out.println("After discount, the result is "+finalPrice);
	}
	
	public void validateDiscount(){
		if (discount<=0){
			System.out.println("No Discount!");
			}	
	}	
		
}